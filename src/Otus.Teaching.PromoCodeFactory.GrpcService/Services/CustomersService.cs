using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.GrpcService
{
    public class CustomersService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customersRepository, IRepository<Preference> preferenceRepository)
        {
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GetAllReply> GetAllCustomers(Empty request, ServerCallContext context)
        {
            var customers = await _customersRepository.GetAllAsync();
            var response = new GetAllReply();
            response.Customers.AddRange(customers.Select(c => new CustomerShortReply
            {
                Id = c.Id.ToString(),
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }));

            return response;
        }

        public override async Task<CustomerReply> GetCustomer(GetRequest request, ServerCallContext context)
        {
            var customer = await _customersRepository.GetByIdAsync(Guid.Parse(request.Id));
            return ToCustomerReply(customer);
        }

        public override async Task<CustomerReply> CreateCustomer(CreateRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());
            var customer = MapToCustomerCreate(request, preferences);

            await _customersRepository.AddAsync(customer);

            return ToCustomerReply(customer);
        }

        public override async Task<CustomerReply> EditCustomer(EditRequest request, ServerCallContext context)
        {
            var customer = await _customersRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
            {
                return new CustomerReply();
            }

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(Guid.Parse).ToList());


            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = preferences.Select(x => new CustomerPreference
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            //customer = MapToCustomerEdit(request, preferences);
            await _customersRepository.UpdateAsync(customer);
            return ToCustomerReply(customer);
        }

        public override async Task<Empty> DeleteCustomer(GetRequest request, ServerCallContext context)
        {
            var customer = await _customersRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
            {
                return new Empty();
            }

            await _customersRepository.DeleteAsync(customer);

            return new Empty();
        }

        public CustomerReply ToCustomerReply(Customer customer)
        {
            var result = new CustomerReply
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };

            if (customer.Preferences != null && customer.Preferences.Any())
            {
                result.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceReply
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                }));
            }

            return result;
        }


        public Customer MapToCustomerCreate(CreateRequest request, IEnumerable<Preference> preferences)
        {
            var id = Guid.NewGuid();

            return new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference
                {
                    CustomerId = id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };
        }

        public Customer MapToCustomerEdit(EditRequest request, IEnumerable<Preference> preferences)
        {
            var id = Guid.Parse(request.Id);

            

            return new Customer
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference
                {
                    CustomerId = id,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };
        }



    }
}
