﻿using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/customers")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();


            var customers = await connection.InvokeAsync<CustomerShortResponse[]>("GetAllCustomers");

            Console.WriteLine("All Customers:");
            foreach (var c in customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }


            var id = customers.FirstOrDefault()?.Id;
            var customer = await connection.InvokeAsync<CustomerResponse>("GetCustomer", id);

            Console.WriteLine("-------");
            Console.WriteLine("Concrete customer:");
            Console.WriteLine($"{customer.Id} {customer.FirstName} {customer.LastName} {customer.Email}");


            var createRequest = new CreateOrEditCustomerRequest
            {
                Email = "grib@mail.ru",
                FirstName = "V",
                LastName = "Lenin",
                PreferenceIds = customer.Preferences.Select(p => p.Id).Take(1).ToList()
            };

            var customerCreate = await connection.InvokeAsync<CustomerResponse>("CreateCustomer", createRequest);
            customers = await connection.InvokeAsync<CustomerShortResponse[]>("GetAllCustomers");

            Console.WriteLine("-------");
            Console.WriteLine("All Customers with added:");
            foreach (var c in customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }

            var editRequest = new CreateOrEditCustomerRequest
            {
                Email = "gribok@mail.ru",
                FirstName = "Vl",
                LastName = "Lenin"
                
            };
            var customerUpdate = await connection.InvokeAsync<CustomerResponse>("EditCustomer",  editRequest, customerCreate.Id);


            customers = await connection.InvokeAsync<CustomerShortResponse[]>("GetAllCustomers");

            Console.WriteLine("-------");
            Console.WriteLine("All Customers with edited:");
            foreach (var c in customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }

            await connection.InvokeAsync("DeleteCustomer", customer.Id);

            Console.WriteLine("-------");
            Console.WriteLine("All Customers with deleted:");
            customers = await connection.InvokeAsync<CustomerShortResponse[]>("GetAllCustomers");
            foreach (var c in customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }


            Console.ReadLine();
            await connection.StopAsync();
        }

    }
}
