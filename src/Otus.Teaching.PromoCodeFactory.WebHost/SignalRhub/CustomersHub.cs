
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.SignalRhub
{
    public class CustomersHub : Hub
    {
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersHub(IRepository<Customer> customersRepository, IRepository<Preference> preferenceRepository)
        {
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
        }

        public  async Task<IEnumerable<CustomerShortResponse>> GetAllCustomers()
        {
            var customers = await _customersRepository.GetAllAsync();
            var response = customers.Select(c => new CustomerShortResponse
            {
                Id = c.Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }).ToList();

            return response;
        }

        public async Task<CustomerResponse> GetCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            var response = new CustomerResponse(customer);
            return response;
        }

        public async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            //�������� ������������ �� �� � ��������� ������� ������
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customersRepository.AddAsync(customer);

            return ToCustomerResponse(customer);
        }

        public async Task<CustomerResponse> EditCustomer(CreateOrEditCustomerRequest request, Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return null;
            }
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            if (request.PreferenceIds == null) return null;

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);



            customer.Preferences = preferences.Select(x => new CustomerPreference
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            //customer = MapToCustomerEdit(request, preferences);
            await _customersRepository.UpdateAsync(customer);
            return ToCustomerResponse(customer);
        }

        public  async Task DeleteCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return;
            }

            await _customersRepository.DeleteAsync(customer);

            return;
        }

        public CustomerResponse ToCustomerResponse(Customer customer)
        {
            return new CustomerResponse
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences.Select(x =>
                    new PreferenceResponse
                    {
                        Id = x.Preference.Id,
                        Name = x.Preference.Name
                    })
                    .ToList(),
            };

        }


        //public Customer MapToCustomerCreate(CreateRequest request, IEnumerable<Preference> preferences)
        //{
        //    var id = Guid.NewGuid();

        //    return new Customer
        //    {
        //        Id = id,
        //        FirstName = request.FirstName,
        //        LastName = request.LastName,
        //        Email = request.Email,
        //        Preferences = preferences.Select(x => new CustomerPreference
        //        {
        //            CustomerId = id,
        //            Preference = x,
        //            PreferenceId = x.Id
        //        }).ToList()
        //    };
        //}

        //public Customer MapToCustomerEdit(EditRequest request, IEnumerable<Preference> preferences)
        //{
        //    var id = Guid.Parse(request.Id);

            

        //    return new Customer
        //    {
        //        Id = id,
        //        FirstName = request.FirstName,
        //        LastName = request.LastName,
        //        Email = request.Email,
        //        Preferences = preferences.Select(x => new CustomerPreference
        //        {
        //            CustomerId = id,
        //            Preference = x,
        //            PreferenceId = x.Id
        //        }).ToList()
        //    };
        //}



    }
}
