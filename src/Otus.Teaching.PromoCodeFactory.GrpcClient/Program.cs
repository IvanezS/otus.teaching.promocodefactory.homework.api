﻿using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GrpcService;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Customers.CustomersClient(channel);

            var customers = await client.GetAllCustomersAsync(new Empty());

            Console.WriteLine("All Customers:");
            foreach (var c in customers.Customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }


            var id = customers.Customers.FirstOrDefault()?.Id;
            var customer = await client.GetCustomerAsync(new GetRequest { Id = id });

            Console.WriteLine("-------");
            Console.WriteLine("Concrete customer:");
            Console.WriteLine($"{customer.Id} {customer.FirstName} {customer.LastName} {customer.Email}");


            var createRequest = new CreateRequest
            {
                Email = "grib@mail.ru",
                FirstName = "V",
                LastName = "Lenin"
            };
            
            createRequest.PreferenceIds.AddRange(customer.Preferences.Select(p => p.Id).Take(1));
            var customerCreate = await client.CreateCustomerAsync(createRequest);
            customers = await client.GetAllCustomersAsync(new Empty());

            Console.WriteLine("-------");
            Console.WriteLine("All Customers with added:");
            foreach (var c in customers.Customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }

            var editRequest = new EditRequest
            {
                Email = "gribok@mail.ru",
                FirstName = "Vl",
                LastName = "Lenin",
                Id = customerCreate.Id
            };
            var customerUpdate = await client.EditCustomerAsync(editRequest);

            customers = await client.GetAllCustomersAsync(new Empty());

            Console.WriteLine("-------");
            Console.WriteLine("All Customers with edited:");
            foreach (var c in customers.Customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }


            var deleteRequest = new GetRequest
            {
                Id = customerCreate.Id
            };
            await client.DeleteCustomerAsync(deleteRequest);
            Console.WriteLine("-------");
            Console.WriteLine("All Customers with deleted:");
            customers = await client.GetAllCustomersAsync(new Empty());
            foreach (var c in customers.Customers)
            {
                Console.WriteLine($"{c.Id} {c.FirstName} {c.LastName} {c.Email}");
            }


            Console.ReadLine();
        }



    }
}
